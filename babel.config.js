module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "module-resolver",
        {
          root: ["./src"],
          extensions: [".js", ".jsx", ".json", ".svg", ".png"],
          // Note: you do not need to provide aliases for same-name paths immediately under /src/
          alias: {
            src: "./src/",
          },
        },
      ],
    ],
  };
};
