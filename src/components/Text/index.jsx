import { StyleSheet, Text } from "react-native";

const StyleText = StyleSheet.create({
  H1: {
    fontSize: 30,
    fontFamily: "system-ui",
    fontWeight: "800",
  },
  H2: {
    fontSize: 28,
    fontFamily: "system-ui",
    fontWeight: "700",
  },
  H3: {
    fontSize: 26,
    fontFamily: "system-ui",
    fontWeight: "600",
  },
  H4: {
    fontSize: 24,
    fontFamily: "system-ui",
    fontWeight: "500",
  },
  H5: {
    fontSize: 22,
    fontFamily: "system-ui",
    fontWeight: "400",
  },
  H6: {
    fontSize: 20,
    fontFamily: "system-ui",
    fontWeight: "400",
  },
  P: {
    fontSize: 16,
    fontFamily: "system-ui",
    fontWeight: "400",
  },
});

export const H1 = ({ children }) => {
  return <Text style={StyleText.H1}>{children}</Text>;
};

export const H2 = ({ children }) => {
  return <Text style={StyleText.H2}>{children}</Text>;
};

export const H3 = ({ children }) => {
  return <Text style={StyleText.H3}>{children}</Text>;
};

export const H4 = ({ children }) => {
  return <Text style={StyleText.H4}>{children}</Text>;
};

export const H5 = ({ children }) => {
  return <Text style={StyleText.H5}>{children}</Text>;
};

export const H6 = ({ children }) => {
  return <Text style={StyleText.H6}>{children}</Text>;
};

export const P = ({ children }) => {
  return <Text style={StyleText.P}>{children}</Text>;
};
