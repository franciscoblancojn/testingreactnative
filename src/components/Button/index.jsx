import { StyleSheet, TouchableWithoutFeedback, Text } from "react-native";

const StyleText = StyleSheet.create({
  Button: {
    fontSize: 30,
    fontFamily: "system-ui",
    fontWeight: "800",
  },
});

export const Button = ({ children, onClick = () => {} }) => {
  return (
    <TouchableWithoutFeedback onPress={onClick}>
      <Text style={StyleText.Button}>{children}</Text>
    </TouchableWithoutFeedback>
  );
};
