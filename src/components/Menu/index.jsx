import {
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
  Text,
  View,
} from "react-native";
import React, { useState, useEffect } from "react";

const StyleMenu = StyleSheet.create({
  Menu: {
    position: "absolute",
    inset: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "#000",
  },
  Btn: {
    position: "absolute",
    top: 10,
    left: 10,
    width: 40,
    height: 30,
    zIndex: 25,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  BtnLine: {
    borderBottomWidth: 5,
    borderBottomColor: "#000",
  },
});

export const Menu = ({}) => {
  const [activeMenu, setActiveMenu] = useState(false);
  const showMenu = () => {
    setActiveMenu(!activeMenu);
  };
  return (
    <>
      <TouchableWithoutFeedback onPress={showMenu}>
        <View style={StyleMenu.Btn}>
          <Text style={StyleMenu.BtnLine}></Text>
          <Text style={StyleMenu.BtnLine}></Text>
          <Text style={StyleMenu.BtnLine}></Text>
        </View>
      </TouchableWithoutFeedback>
      <ScrollView
        style={[
          StyleMenu.Menu,
          {
            transform: [{ translateX: activeMenu ? "0%" : "-100%" }],
          },
        ]}
      ></ScrollView>
    </>
  );
};
