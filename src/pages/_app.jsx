import { StatusBar } from "expo-status-bar";
import Constants from "expo-constants";
import { Alert, ScrollView } from "react-native";
import { H1 } from "src/components/Text";
import { Button } from "src/components/Button";
import { Menu } from "src/components/Menu";

const Index = () => {
  return (
    <>
      <Menu />
      <ScrollView style={{ paddingTop: Constants.statusBarHeight }}>
        <H1>Open up App.js to start working on your app!</H1>
        <Button
          onClick={() => {
            Alert.alert("alert");
          }}
        >
          {" "}
          Hola
        </Button>
        <StatusBar style="auto" />
      </ScrollView>
    </>
  );
};
export default Index;
